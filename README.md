# Doghouse Update Helpers

## What it does
  - Provides helpers for your update modules
  -- Taxonomy creator helper

## How to install
  - Add `ssh://git@stash.dhmedia.com.au:7999/ddmodules/doghouse-update-helpers-drupal-8.git` to `composer.json`
```
"repositories": [
        ...
        {
            "type": "git",
            "url": "ssh://git@stash.dhmedia.com.au:7999/ddmodules/doghouse-update-helpers-drupal-8.git"
        }
```
  - Add the module under `require`
```
"require": {
    ...
    "doghouse/doghouse_update_helpers": "2.0.1",
```
  - In terminal, run: `$ composer require doghouse/doghouse_update_helpers`
  - Enable the module (EG `drush en doghouse_update_helpers`) or add it as a dependency to your update module

## Contribute!
  - Make this module better, add helpers that everyone will love!!
